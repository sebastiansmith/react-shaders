/* eslint-env node, mocha */
/* global expect */
/* eslint no-console: 0 */
'use strict';

// Uncomment the following lines to use the react test utilities
// import TestUtils from 'react-addons-test-utils';
import createComponent from 'helpers/shallowRenderHelper';

import PlaneComponent from 'components//PlaneComponent.js';

describe('PlaneComponent', () => {
  let component;

  beforeEach(() => {
    component = createComponent(PlaneComponent);
  });

  it('should have its component name as default className', () => {
    expect(component.props.className).to.equal('plane-component');
  });
});

'use strict';

import React from 'react';
import React3 from 'react-three-renderer';
import * as THREE from 'three';
import ShaderSphereComponent from './ShaderSphereComponent.js'
import { Vector2 } from 'three';

require('styles//Three.css');

class SphereContainer extends React.Component {
   constructor(props, context) {
    super(props, context);

    // construct the position vector here, because if we use 'new' within render,
    // React will think that things have changed when they have not.
    this.cameraPosition = new THREE.Vector3(0, 0, 300);

    this.state = {
      cubeRotation: new THREE.Euler(),
      targetPosition: new THREE.Vector3(0, 0, -1000),
      amplitude: 0,
      frame: 0,
      mouse: new Vector2(0,0)
    };

    this.frame = 0;

    this._onAnimate = () => {
      // we will get this callback every frame pretend cubeRotation is immutable. this
      // helps with updates and pure rendering. React will be sure that the rotation
      // has now updated.
    

      this.setState({
        cubeRotation: new THREE.Euler(this.state.cubeRotation.x + 0.1, this.state.cubeRotation.y + 0.1, 0),
        amplitude: Math.sin(this.frame),
        frame: this.frame
      });

      // update the frame counter
      this.frame += 0.1;
    };
}

onMouseMove(e) {
  this.setState({ mouse: new Vector2(e.screenX,e.screenY)});
}

  render() {
    const width = window.innerWidth; // canvas width
    const height = window.innerHeight - 5; // canvas height

    return (
      <div onMouseMove={this.onMouseMove.bind(this)}>
      <React3 ref="renderer" mainCamera="camera" width={width} height={height} onAnimate={this._onAnimate} clearColor={0x000000} alpha={true} >
        <scene>
          <perspectiveCamera
            name="camera"
            fov={75}
            aspect={width / height}
            near={1}
            far={6000}
            lookAt={this.state.targetPosition}
            position={this.cameraPosition}/>
          <ShaderSphereComponent amplitude={this.state.amplitude} time={this.state.frame} mouse={this.state.mouse} width={width} height={height}/>
        </scene>
      </React3>
      </div>
    );
  }
}

SphereContainer.displayName = 'SphereContainer';

// Uncomment properties you need
// SphereContainer.propTypes = {};
// SphereContainer.defaultProps = {};

export default SphereContainer;

import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';

import { withRouter } from 'react-router-dom'
require('styles/Nav.css');

class NavComponent extends React.Component {
  constructor(props, context) {
   super(props, context);

   this.state = {
     selectedIndex:0
   }
  }

  componentWillMount()
  {
    if(this.props.history.location.pathname == '/sphere')
    {
      this.setState({selectedIndex: 0});
    }
    else if(this.props.history.location.pathname == '/plane')
    {
      this.setState({selectedIndex: 1});
    }
  }

  render() {
    return(
      <Tabs className="nav-component" initialSelectedIndex={this.state.selectedIndex}>
      <Tab
        label="Sphere"
        data-route="/sphere"
        onActive={() => this.props.history.push('/sphere')}
      >
      </Tab>
      <Tab
        label="Plane"
        data-route="/plane"
        onActive={() => this.props.history.push('/plane')}
      >
      </Tab>
    </Tabs>
    )
  }
}

export default withRouter(NavComponent);
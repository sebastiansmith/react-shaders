'use strict';

import React from 'react';
import * as THREE from 'three';

require('styles//Shader.css');

class ShaderSphereComponent extends React.Component {
 constructor(props, context) {
    super(props, context);

    this.state = {
      position: null,
      color: null,
      scenePosition: new THREE.Vector3(0, 0, 0),
      texture: ''
    }
  }

 init()
  {
    var displacement = [];
    for(var i=0; i< this.refs.geometry.attributes.position.count;i++)
    {
      displacement.push(Math.random() * 40* i/this.refs.geometry.attributes.position.count);
    }

    this.refs.geometry.addAttribute('displacement', new THREE.Float32BufferAttribute( displacement, 1 ));
    this.refs.geometry.attributes.displacement.needsUpdate = true;
  }

  componentDidMount()
  {
    this.init();
  }

  render() {
        const vertexShader = document
      .getElementById('vertexshader')
      .textContent;
    const fragmentShader = document
      .getElementById('fragmentshader')
      .textContent;

    return (
     <mesh position={this.state.scenePosition}  ref='shadersphere'>
        <shaderMaterial
          vertexShader={vertexShader}
          fragmentShader={fragmentShader}
         >
          <uniforms ref="uniforms">
            <uniform name='texture' value={this.state.texture} type={'t'}></uniform>
            <uniform name='amplitude' value={this.props.amplitude} type={'f'}></uniform>
            <uniform name='u_time' value={this.props.time} type={'f'}></uniform>
            <uniform name='u_resolution' value={{ x: this.props.width, y: this.props.height}} type={'v2'}></uniform>
            <uniform name='u_mouse' value={{ x: this.props.mouse.x, y: this.props.mouse.y}} type={'v2'}></uniform>
          </uniforms>
        </shaderMaterial>
        <sphereGeometry ref="geometry" radius={30} heightSegments={30} widthSegments={30}>
        </sphereGeometry>
      </mesh>
    );
  }
}

ShaderSphereComponent.displayName = 'ShaderSphereComponent';

// Uncomment properties you need
// ShaderSphereComponent.propTypes = {};
// ShaderSphereComponent.defaultProps = {};

export default ShaderSphereComponent;

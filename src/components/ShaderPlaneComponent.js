'use strict';

import React from 'react';
import * as THREE from 'three';

require('styles//Shader.css');

class ShaderPlaneComponent extends React.Component {
 constructor(props, context) {
    super(props, context);

    this.state = {
      position: null,
      color: null,
      scenePosition: new THREE.Vector3(0, 0, 0),
      texture: ''
    }
  }

  render() {
        const vertexShader = document
      .getElementById('vertexshader-plane')
      .textContent;
    const fragmentShader = document
      .getElementById('fragmentshader-plane')
      .textContent;

    return (
     <mesh position={this.state.scenePosition}  ref='shaderplane'>
        <shaderMaterial
          vertexShader={vertexShader}
          fragmentShader={fragmentShader}
         >
          <uniforms ref="uniforms">
            <uniform name='texture' value={this.state.texture} type={'t'}></uniform>
            <uniform name='amplitude' value={this.props.amplitude} type={'f'}></uniform>
            <uniform name='u_time' value={this.props.time} type={'f'}></uniform>
            <uniform name='u_resolution' value={{ x: this.props.width, y: this.props.height}} type={'v2'}></uniform>
            <uniform name='u_mouse' value={{ x: this.props.mouse.x, y: this.props.mouse.y}} type={'v2'}></uniform>
            <uniform name='u_heartrate' value={this.props.heartRate} type={'f'}></uniform>
            <uniform name='u_heartrateset' value={this.props.heartRateSet} type={'fv1'}></uniform>
          </uniforms>
        </shaderMaterial>
        <planeBufferGeometry ref="geometry" width={2} height={2}>
        </planeBufferGeometry>
      </mesh>
    );
  }
}

ShaderPlaneComponent.displayName = 'ShaderPlaneComponent';

// Uncomment properties you need
// ShaderComponent.propTypes = {};
// ShaderComponent.defaultProps = {};

export default ShaderPlaneComponent;

require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import {Route, Switch} from 'react-router-dom'
import SphereContainer from './SphereContainer';
import PlaneContainer from './PlaneContainer';
import NavComponent from './NavComponent';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class AppComponent extends React.Component {
  render() {
    return (
      <div>
      <MuiThemeProvider>
        <NavComponent/>
      </MuiThemeProvider>
      <div>
        <Switch>
          <Route path="/plane" component={PlaneContainer}/>
          <Route path="/" component={SphereContainer}/>
        </Switch>
      </div>
    </div>
    );
  }
}

AppComponent.defaultProps = {
};

export default AppComponent;

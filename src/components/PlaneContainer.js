'use strict';

import React from 'react';
import React3 from 'react-three-renderer';
import * as THREE from 'three';
import ShaderPlaneComponent from './ShaderPlaneComponent.js'
import { Vector2 } from 'three';

require('styles//Three.css');

const dat = require('dat.gui');

class PlaneContainer extends React.Component {
   constructor(props, context) {
    super(props, context);

    // construct the position vector here, because if we use 'new' within render,
    // React will think that things have changed when they have not.
    this.cameraPosition = new THREE.Vector3(0, 0, 300);

    this.state = {
      cubeRotation: new THREE.Euler(),
      targetPosition: new THREE.Vector3(0, 0, -1000),
      amplitude: 0,
      frame: 0,
      mouse: new Vector2(0,0),
      heartRate: 100,
      heartRateSet: new Array(360).fill(0),
      rates: new Array(360).fill(null).map(() =>{ return Math.random()*2+50} )
    };

    this.frame = 0;
    this.gui = new dat.GUI();
    this.heartRateSet = [];

    this.options = {
      rate: 100.0
    }
 
    this._onAnimate = () => {
      // we will get this callback every frame pretend cubeRotation is immutable. this
      // helps with updates and pure rendering. React will be sure that the rotation
      // has now updated.
    
    
      this.frame += 0.1;
      // update the frame counter
    };
}

componentDidMount()
{
  var inputs = this.gui.addFolder('Inputs');
  inputs.add(this.options,'rate', 0, 200.0).listen();
  inputs.open();
  var self = this;
  this.timer = setInterval(function() {
    self.options.rate = self.options.rate + (Math.random()<.5 ? 1 : -1);
  }, 1000);

  this.setHeartRates();
}

onMouseMove(e) {
  this.setState({ mouse: new Vector2(e.screenX,e.screenY)});
}
distance(a, b) {
  a = 360+a % 360;
  b = 360+b % 360;
  if (a>b) {
    return a-b;
  } else {
    return b-a;
  }
}
setHeartRates() {
  
   let currentHeartRateSet = this.state.heartRateSet;
   let index = Math.round(this.frame * 10.0) % 360;
   let d = 20;
   for (var i=0;i<currentHeartRateSet.length;i++) {
     let x = this.distance( i, index);
     let xd = 0;
     if (x<d) {
      xd = this.options.rate*.6  * (1-x/d);
     } 
      //currentHeartRateSet[i] = this.state.rates[i]*Math.abs(Math.sin(this.frame+i*.008)) + xd;
      currentHeartRateSet[i] = xd;
   }

  //currentHeartRateSet[index] = this.options.rate;

  // let j = 0;
  // let len = 150;
  // while (j<len) {
  //   currentHeartRateSet[(index+j+1)%currentHeartRateSet.length] = Math.max(0, this.options.rate-(j+1)*.5);
  //   // currentHeartRateSet[(index-(j+1))<0 ? currentHeartRateSet.length-1 : (index-(j+1))];
  //   j+=1;
  // }
 // console.log(currentHeartRateSet[index])
  this.setState({
    cubeRotation: new THREE.Euler(this.state.cubeRotation.x + 0.1, this.state.cubeRotation.y + 1, 0),
    amplitude: Math.sin(this.frame),
    frame: this.frame,
    heartRate: this.options.rate, //Math.random()<.2 ? (Math.random()*2-1)+this.state.heartRate : this.state.heartRate // howard - update with real data here
    heartRateSet: currentHeartRateSet
  });
  
 //console.log(index, this.state.heartRateSet);
 var self = this;
 this.heart_timer = setInterval(function(){self.setHeartRates();}, 1000/30);
 
}

  render() {
    const width = window.innerWidth; // canvas width
    const height = window.innerHeight - 5; // canvas height
   

    return (
      <div onMouseMove={this.onMouseMove.bind(this)}>
      <React3 ref="renderer" mainCamera="camera" // this points to the perspectiveCamera which has the name set to "camera" below
  width={width} height={height} onAnimate={this._onAnimate} clearColor={0x000000} alpha={true} >
        <scene>
          <perspectiveCamera
            name="camera"
            fov={75}
            aspect={width / height}
            near={1}
            far={6000}
            lookAt={this.state.targetPosition}
            position={this.cameraPosition}/>
          <ShaderPlaneComponent amplitude={this.state.amplitude} heartRate={this.state.heartRate} heartRateSet={this.state.heartRateSet} time={this.state.frame} mouse={this.state.mouse} width={width} height={height}/>
        </scene>
      </React3>
      </div>
    );
  }
}

PlaneContainer.displayName = 'PlaneContainer';



export default PlaneContainer;

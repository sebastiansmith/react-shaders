# README #


### What is this repository for? ###

A framework to learn shader coding that also embeds Three.js within react.

### References ###

Shader code initially based on this set of tutorials: https://aerotwist.com/tutorials/an-introduction-to-shaders-part-1/

Uses this port of Three.JS components into React: https://github.com/toxicFork/react-three-renderer

Project structure based on this yeoman template: https://github.com/react-webpack-generators/generator-react-webpack#readme

### How do I get set up? ###

simply run npm install then npm start



<!--   vec2 st = gl_FragCoord.xy/u_resolution.xy;;
    float multiplier = 1.1;
    vec3 rect1 = rectangle(0.0,0.6,0.8,0.0,st, multiplier);
    vec3 rect2 = rectangle(0.13,0.6,0.6,0.0,st, multiplier);
    vec3 rect3 = rectangle(0.0,0.15,0.8,0.35,st, multiplier);
    vec3 rect4 = rectangle(0.13,0.15,0.6,0.35,st, multiplier);

    vec3 color = vec3(max(max(rect1,rect2),max(rect3, rect4)));

    gl_FragColor = vec4(color.x * (1.0 - rect2.x * rect2.y),color.y * (1.0 - rect1.x * rect1.y) * (1.0 - rect2.x * rect2.y),color.z * (1.0 - rect1.x * rect1.y),1.0);
   -->